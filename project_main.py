import os
import sys
import time
import numpy as np
import pickle
from keras.applications import VGG19
from keras.preprocessing import image
from keras.applications.vgg19 import preprocess_input
from sklearn.neighbors import NearestNeighbors

class my_model():

    def __init__(self, DATA_PATH, FILENAME, NUM_OF_IMAGES):

        self.FILENAME = FILENAME
        self.TARGET_SIZE = (150,150)
        self.DATA_PATH = DATA_PATH
        self.model = VGG19(weights='imagenet', include_top=False)
        self.images = [file for file in os.listdir(DATA_PATH)[:NUM_OF_IMAGES]]

    def to_vector(self, file, path = ""):
        file_path = os.path.join(path, file)
        img = image.load_img(file_path, target_size = self.TARGET_SIZE) 
        x = image.img_to_array(img)  
        x = np.expand_dims(x, axis=0) 
        x = preprocess_input(x) 
        return self.model.predict(x).ravel()

    def predict(self, TARGET = "1.jpg", N_NEIGHBORS =10):
        try:
            with open(self.FILENAME, "rb") as f:
                knn = pickle.load(f)
        except:
            print("Файл не найден")
        target_vec = self.to_vector(TARGET)
        target_vec = target_vec.reshape(target_vec.shape[0], 1).T
        dist, indices = knn.kneighbors(target_vec, n_neighbors = N_NEIGHBORS)
        dist = dist[0]
        indices = indices[0]
        similar_images = [(self.images[indices[i]], dist[i]) for i in range(len(indices))]
        return similar_images

    def fit(self):
        try:
            vectors = []
            counter = 1
            for file in self.images:
                try:
                    vector = self.to_vector(file, path = self.DATA_PATH)
                except:
                    os.remove(os.path.join(self.DATA_PATH, file))
                    print(f'deleted {file}')
                else:
                    vector = self.to_vector(file, path = self.DATA_PATH)
                    vectors.append(vector)
                    print(counter,file)
                    counter+=1
            try:
                with open(self.FILENAME, "rb") as f:
                    knn = pickle.load(f)
            except:
                knn = NearestNeighbors(metric='cosine', algorithm='brute')
            knn.fit(vectors)
            with open(self.FILENAME, "wb") as f:
                pickle.dump(knn,f)
        except:
            with open("lister", "wb") as f:
                pickle.dump(vectors,f)
            print("я упал")
            knn = NearestNeighbors(metric='cosine', algorithm='brute')
            knn.fit(vectors)
            with open(self.FILENAME, "wb") as f:
                pickle.dump(knn,f)



