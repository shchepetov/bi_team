from PIL import Image, ImageFilter
import os 

OLD_PATH = "true_img"
NEW_PATH = "resized_"
images = [file for file in os.listdir(OLD_PATH)]

counter = 1
d = 3
newfile = "resized_3"
for file in images:
	try:
		path = os.path.join(OLD_PATH, file)
		img = Image.open(path)
		w,h = img.size
		to_crop = []
		a = None
		for x in range(w):
			for y in range(h):
				if img.getpixel((x, y)) != (255,255,255):
					a = x
					break
			if a!=None:

				to_crop.append(a)
				break
		a = None
		for y in range(h):
			for x in range(w):
				if img.getpixel((x, y)) != (255,255,255):
					a = y
					break
			if a!=None:
				to_crop.append(a)
				break
		a = None
		for x in range(w-1,0,-1):
			for y in range(h):
				if img.getpixel((x, y)) != (255,255,255):
					a = x
					break
			if a!=None:

				to_crop.append(a)
				break
		a = None
		for y in range(h-13,-1,-1):
			for x in range(w):
				if img.getpixel((x, y)) != (255,255,255):
					a = y
					break
			if a!=None:
				to_crop.append(a)
				break
		img2 = img.crop(to_crop)
		if counter>=10000:
			os.mkdir(os.path.join(f'{NEW_PATH}{d}'))
			print(newfile)
			d+=1
			counter = 0
		newfile = os.path.join(f'{NEW_PATH}{d}', file)
		try:
			img2.save(newfile)
		except OSError:
			img2.convert("RGB").save(newfile)
		except Exception as e:
			print(e)
			print("пропуск")
			continue
		print(counter)
		counter+=1
	except:
		print('пропуск')

