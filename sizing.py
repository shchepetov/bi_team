import os
from PIL import Image, UnidentifiedImageError 


def resize(images, old_path = "", new_path = "", w=200, h=200):

    for file in images:
        old_file = os.path.join(old_path, file)
        new_file = os.path.join(new_path, file)
        try:
            old_im = Image.open(old_file)
            old_size = old_im.size
            if old_size[0]< w and  old_size[0]<= h:
                new_size = (w, h)
                new_im = Image.new("RGB", new_size,'#ffffff')   ## luckily, this is already black!
                new_im.paste(old_im, (int((new_size[0]-old_size[0])/2),int((new_size[1]-old_size[1])/2)))
                # print(new_im.show())
                new_im.save(new_file)
        except:
            pass


def max_size(
        images, 
        path = "") -> tuple:
        
    widths = []
    heights = []
    for file in images:
        try:
            img = Image.open(os.path.join(path, file))
        except UnidentifiedImageError:
            pass
        else:
            w, h = img.size
            widths.append(w)
            heights.append(h)
    return max(widths), max(heights)

if __name__ == '__main__':
    
    OLD_PATH = "true_img/"
    NEW_PATH = "new/"
    EXTS = [
        "jpg",
        "jpeg",
        "png",
        "bmp",
        ]

    images = [file for file in os.listdir(OLD_PATH) if file.split(".")[-1] in EXTS]
    #max_w, max_h = max_size(
    #                        images,
    #                        path = OLD_PATH)

    resize(
        images,
        old_path = OLD_PATH,
        new_path = NEW_PATH,
        w = 500,
        h = 500)