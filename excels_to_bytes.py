import pandas as pd
import pickle
import os

FILENAME = "saved_excels"
FOLDER = "excel/"

STE_VSE = pd.read_excel(os.path.join(FOLDER, 'Kh_Ste_Vse.xlsx'), delimiter=',')
OFFERTI = pd.read_excel(os.path.join(FOLDER,'Kh_Oferty.xlsx'), delimiter=',')
POSTAVKA = pd.read_excel(os.path.join(FOLDER, 'Kh_Postavschiki.xlsx'), delimiter=',')

with open(FILENAME, "wb") as f:
	pickle.dump((STE_VSE, OFFERTI, POSTAVKA,), f)
